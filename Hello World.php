<?php
class MyParentClass
{
    private $helloWorld= "Hello";

    public function setHelloWorld($helloWorld)
    {
        $this->helloWorld = $helloWorld;
    }

    public function getHelloWorld()
    {
        return $this->helloWorld;
    }
}
$object1MyParentClass_Golpahar = new MyParentClass();
echo $object1MyParentClass_Golpahar->getHelloWorld()."<br>";

$object1MyParentClass_Golpahar->setHelloWorld($object1MyParentClass_Golpahar->getHelloWorld()."World");
echo $object1MyParentClass_Golpahar->getHelloWorld()."<br>";



