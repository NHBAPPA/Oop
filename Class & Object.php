<?php

class MyParentClass
{
    public $imAPublicProperty = "Public Property";
    protected $imAProtectedProperty = "protected Property";
    private $imAPrivateProperty = "privates Property";

    private $helloWorld= "Hello";

    public function setHelloWorld($helloWorld)
    {
        $this->helloWorld = $helloWorld;
    }

    public function getHelloWorld()
    {
        return $this->helloWorld;
    }

    public function setImAPrivateProperty($imAPrivateProperty)
    {
        $this->imAPrivateProperty = $imAPrivateProperty;
    }

    public function getImAPrivateProperty()
    {
        return $this->imAPrivateProperty;
    }
public function doSomethingPublic(){
    echo "Now I am inside the ".__METHOD__. "<br>";
    $this->doSomethingPrivate();
}
    protected function doSomethingProtected(){
        echo "Now I am inside the".__METHOD__."<br>";
    }

    private function doSomethingPrivate(){
        echo "Now I am inside the".__METHOD__."<br>";
    }
}

$object1MyParentClass_Golpahar = new MyParentClass();
$object2MyParentClass_Agrabad = new MyParentClass();
$object3MyParentClass_GEC = new MyParentClass();

$object1MyParentClass_Golpahar->imAPublicProperty ="Public Property Golpahar";
echo $object1MyParentClass_Golpahar->imAPublicProperty."<br>";

$object1MyParentClass_Golpahar->setImAPrivateProperty("Private Property Golpahar");
echo $object1MyParentClass_Golpahar->getImAPrivateProperty(). "<br>";

$object1MyParentClass_Golpahar->doSomethingPublic();
echo $object1MyParentClass_Golpahar->getHelloWorld()."<br>";

$object1MyParentClass_Golpahar->setHelloWorld($object1MyParentClass_Golpahar->getHelloWorld()."World");
echo $object1MyParentClass_Golpahar->getHelloWorld()."<br>";
