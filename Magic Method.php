<?php
class MyMagicClass
{

    public function __construct()
    {
        echo "Now I am inside the".__METHOD__."<br>";
    }

    public function __destruct()
    {
        echo "Now I am inside the".__METHOD__."<br>";
    }
}

$objectMyMagicClass= new MyMagicClass();