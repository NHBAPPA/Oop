<?php

class MyParentClass
{
    public $imAPublicProperty = "Public Property";
    protected $imAProtectedProperty = "protected Property";
    private $imAPrivateProperty = "privates Property";

    private $helloWorld= "Hello";

    public function setHelloWorld($helloWorld)
    {
        $this->helloWorld = $helloWorld;
    }

    public function getHelloWorld()
    {
        return $this->helloWorld;
    }

    public function setImAPrivateProperty($imAPrivateProperty)
    {
        $this->imAPrivateProperty = $imAPrivateProperty;
    }

    public function getImAPrivateProperty()
    {
        return $this->imAPrivateProperty;
    }
    public function doSomethingPublic(){
        echo "Now I am inside the ".__METHOD__. "<br>";
        $this->doSomethingPrivate();
    }
    protected function doSomethingProtected(){
        echo "Now I am inside the".__METHOD__."<br>";
    }

    private function doSomethingPrivate(){
        echo "Now I am inside the".__METHOD__."<br>";
    }
} //End

class MyChildClass extends MyParentClass{

    public function executeChildClassMethod(){

        echo "imAProtectedProperty =" .$this->imAProtectedProperty;

    }
}

$objMyChildClass=new MyChildClass();
$objMyChildClass->executeChildClassMethod();